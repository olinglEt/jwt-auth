package main

// User ユーザー
type User struct {
	Username    string
	DisplayName string
	Hash        string
}
