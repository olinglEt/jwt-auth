package main

import (
	"crypto/sha256"
	"fmt"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"

	_ "github.com/lib/pq"
)

const secret = "+KQINql2mDNpNe7QJd8++iJjq67s4kiPZ/D1qNYrbudifLQscynBqQ=="

var dao *DbAccessor
var err error

func main() {
	router := gin.Default()
	router.Use(cors.Default())

	dao = new(DbAccessor)
	dao.Connect()
	dao.CreateTable()

	defer dao.Close()

	/* routing */
	router.POST("/signin", signin)
	router.POST("/register", register)
	router.POST("/validate", validate)

	router.Run(":5000")
}

func register(c *gin.Context) {
	username := c.PostForm("username")
	displayName := c.PostForm("display_name")
	password := c.PostForm("password")

	sum := sha256.Sum256([]byte(password))
	err := dao.InsertUser(username, displayName, fmt.Sprintf("%x", sum))

	if err == nil {
		c.JSON(200, gin.H{
			"success": true,
		})
	} else {
		c.JSON(500, gin.H{
			"err": err,
		})
	}
}

func signin(c *gin.Context) {
	username := c.PostForm("username")
	password := c.PostForm("password")

	user, err := dao.SearchUserByUsername(username)
	if err != nil {
		c.JSON(500, gin.H{
			"err": "ユーザーの検索に失敗しました",
		})
		return
	}

	sum := sha256.Sum256([]byte(password))
	if fmt.Sprintf("%x", sum) != user.Hash {
		c.JSON(500, gin.H{
			"err": "ユーザーの認証に失敗しました",
		})
		return
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, &jwt.StandardClaims{
		Id:        username,
		ExpiresAt: time.Now().Add(time.Hour * 1).Unix(),
	})

	tokenString, err := token.SignedString([]byte(secret))

	if err != nil {
		c.JSON(500, gin.H{
			"err": "トークンの作成に失敗しました",
		})
	} else {
		c.JSON(200, gin.H{
			"token": tokenString,
		})
	}
}

func validate(c *gin.Context) {
	token := c.PostForm("token")

	if token == "" {
		c.JSON(500, gin.H{
			"err": "無効なトークンです",
		})
	}

	if username, isValid := validateToken(token); isValid {
		user, _ := dao.SearchUserByUsername(username)
		c.JSON(200, gin.H{
			"Username":    user.Username,
			"DisplayName": user.DisplayName,
		})
	} else {
		c.JSON(500, gin.H{
			"err": "無効なトークンです",
		})
	}
}

func validateToken(tokenString string) (string, bool) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		return []byte(secret), nil
	})

	if err != nil {
		return "", false
	}

	if token.Valid {
		username := token.Claims.(jwt.MapClaims)["jti"]
		return username.(string), true
	}
	return "", false
}
