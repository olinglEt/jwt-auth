package main

import (
	"database/sql"
	"errors"
	"time"

	_ "github.com/lib/pq"
)

// DbAccessor Database Accessor Objectを定義する構造体
type DbAccessor struct {
	client *sql.DB
}

// Connect DBに接続する
func (dao *DbAccessor) Connect() {
	db, err := sql.Open("postgres", "host=db_auth port=5432 user=postgres password=secret sslmode=disable")

	if err != nil {
		// panic(err)
		time.Sleep(time.Second * 10)
		dao.Connect()
	}

	err = db.Ping()
	if err != nil {
		// panic(err)
		time.Sleep(time.Second * 10)
		dao.Connect()
	}
	dao.client = db
}

// ReconnectIfNotConnected  DBの接続がなければ接続し直す
func (dao *DbAccessor) ReconnectIfNotConnected() {
	if dao.client == nil {
		dao.Connect()
	}
}

// CreateTable テーブルが存在しなければ作成する
func (dao *DbAccessor) CreateTable() {
	dao.ReconnectIfNotConnected()
	dao.client.Query("create table if not exists users (" +
		"username      text primary key," +
		"display_name  text," +
		"hash          text" +
		")")
}

// SearchUserByUsername ユーザー名を元にUSERテーブルを検索し、最初のレコードを返す
func (dao *DbAccessor) SearchUserByUsername(username string) (user User, err error) {
	dao.ReconnectIfNotConnected()
	rows, err := dao.client.Query("select * from users where username = $1", username)
	if err != nil {
		return User{}, errors.New("error")
	}

	var resList = []User{}
	defer rows.Close()
	for rows.Next() {
		user := User{}
		err = rows.Scan(&user.Username, &user.DisplayName, &user.Hash)
		if err != nil {
			panic(err)
		}
		resList = append(resList, user)
	}
	if len(resList) == 0 {
		return User{}, errors.New("no such user")
	}
	return resList[0], nil
}

// InsertUser ユーザーを新規登録する
func (dao *DbAccessor) InsertUser(username string, displayName string, hash string) error {
	dao.ReconnectIfNotConnected()

	_, err := dao.client.Exec("insert into users (username, display_name, hash) values ($1, $2, $3)", username, displayName, hash)
	return err
}

// Close 接続を閉じる
func (dao *DbAccessor) Close() {
	if dao.client != nil {
		dao.client.Close()
	}
}
